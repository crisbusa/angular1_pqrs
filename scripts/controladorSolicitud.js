

// 
/*
angular.module('miApp').controller('CtrFormularioPQRS', function ($scope, $http) {
     $scope.prioridades = ['Alta', 'Media', 'Baja'];
     $scope.enviarSolicitud = function() {
         $http.post("https://script.google.com/macros/s/AKfycbw8GuwdSdDDvruuRGcEgu19zXz-Qhn-pJ3MUNigSLYJfYLvzmY/exec",
                     {  solicitud : $scope.solicitud })
                         .success(function (resul) {
                             if (resul.respuestaServidor == "ok"){
                                 $scope.resultado="Se ha enviado con exito";
                                 $scope.solicitud="";
                             }
                        });
               
              };

  }); */

angular.module('miApp').controller('CtrFormularioPQRS', CtrFormularioPQRS);


CtrFormularioPQRS.$inject = ['$http']
  function CtrFormularioPQRS($http) {
    var ctrl = this;
    ctrl.prioridades = ['Alta', 'Media', 'Baja'];
    ctrl.saludo="saludo";
    ctrl.enviarSolicitud =function() {
         $http.post("https://script.google.com/macros/s/AKfycbw8GuwdSdDDvruuRGcEgu19zXz-Qhn-pJ3MUNigSLYJfYLvzmY/exec",
                     {  solicitud : ctrl.solicitud })
                         .success(function (resul) {
                             console.log(resul);
                             if (resul.respuestaServidor == "ok"){
                                 ctrl.resultado="Se ha enviado con exito";
                                 ctrl.solicitud="";
                             }
                        });
              };
}

/*
angular.module('miApp').controller('CtrFormularioPQRS', CtrFormularioPQRS);

CtrFormularioPQRS.$inject = ['consultaServices']
  function CtrFormularioPQRS(consultaServices){
    var ctrl = this;
    ctrl.prioridades = ['Alta', 'Media', 'Baja'];

    ctrl.enviarSolicitud =function() {
      consultaServices.postSolicitud(ctrl.solicitud).then(function (response) {
         console.log(response);
          if (response.data.respuestaServidor == "ok"){
                ctrl.resultado="Se ha enviado con exito";
                ctrl.solicitud="";
            }
      });
    }

  } 
  */


  