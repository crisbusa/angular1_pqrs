
angular.module('miApp').service('consultaServices', consultaServices);

consultaServices.$inject = ['$http']
function consultaServices($http) {
 this.suma = suma
 this.getConsultaSolicitudes = getConsultaSolicitudes
 this.postSolicitud = postSolicitud


  function suma(a, b){
        return parseInt(a) + parseInt(b)
      }
  
  function getConsultaSolicitudes(){ 
       return $http.get("https://script.google.com/macros/s/AKfycbzAHPkqEJoBRtZYQKG_E5GsaE1rl6YUswkhOsvPTKd8UdU_ZFs/exec");
  }

   function postSolicitud(objeto){ 
       return $http.post("https://script.google.com/macros/s/AKfycbw8GuwdSdDDvruuRGcEgu19zXz-Qhn-pJ3MUNigSLYJfYLvzmY/exec"
               ,{solicitud : objeto});
  }

}

