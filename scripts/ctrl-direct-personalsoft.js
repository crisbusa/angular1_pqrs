
/* Una  forma 
angular.module('miApp').directive('personalsoftDirectiva', function () {
    return {
        templateUrl: 'views/direct-personalsoft.html',
        controller: CtrDirectiva
  };
}); */


/*  Otra Forma           */
angular.module('miApp').directive('personalsoftDirectiva', directPersonalsoft);

function directPersonalsoft() {
  return {
    templateUrl: 'views/direct-personalsoft.html',
    controller: CtrDirectiva};
}


angular.module('miApp').controller('CtrDirectiva', CtrDirectiva);

function CtrDirectiva() {
  var ctrl = this;
  ctrl.inicio="Primera Directiva Angular en PersonalSoft";
  ctrl.accion = function() {
        alert("Ingreso la siguiente clave: " + ctrl.clave + " Usuario "+ctrl.email);  
     };

}