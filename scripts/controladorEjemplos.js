
/* 1) Forma  */ 
angular.module('miApp').controller('CtrEjemplos', function ($scope) {

     $scope.saludo = "Este es mi primer ejemplo con Angular JS ";
     $scope.click = 0;
     $scope.variable = false;


     $scope.contarClick = function() {
               $scope.click++; 
                if($scope.click > 5){
                    $scope.variable=true;
                } 
              };

    $scope.restarClick = function() {
               $scope.click--;  
              };

  }); 

  

























/*2) Forma  cuando estamos hay varios controladores en una vista diferenciar el $scope*/
  angular.module('miApp').controller('CtrEjemplos1', CtrEjemplos1);

//  1: $scope no es inyectado en el controlador
function CtrEjemplos1() {
  // 2: Se instancia  la variable que reemplaza al $scope
  var ctrl = this;
  //  3: se reemplaza todo $scope con ctrl
  ctrl.saludo = "Este es mi primer ejemplo con Angular JS ";
  ctrl.click = 0;

  ctrl.contarClick = function() {
               ctrl.click++;  
              };

    ctrl.restarClick = function() {
               ctrl.click--;  
              };

}


